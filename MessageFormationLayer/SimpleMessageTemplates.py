


MessageTemplates={

    "simple":{
        "createMessageGreeting":{
            "basic":[
                "Welcome to <b>zingit</b> solution "
            ],

            "namePresent": [
                "Thank you! Bye!",

            ],
            "askForUserForm": [
                "Awesome, please fill in your details and we will get back to you soonest.",

            ],
            "CatPresent": [
                "Are you an existing customer ?"
            ],
            "catSelected":[

                "are you looking for a new solution?"
            ],
            "selectRegions": [
                "Please select the category"

            ],
            "durationSubCatPresent": [
                "How long you want the tour to be?"
            ],
             "awsome":[
                 "Awesome!"
             ],
             "about":[
                 "How may I help you?",

             ],
            "aboutIndia": [
                "India's culture is among the world's oldest; civilization in India began about 4,500 years ago.",

            ],
            "aboutIndia2": [
                " Facts about international travel are:",

            ],
            "aboutIndia3": [
                "1) The number of Indians travelling abroad has gone up 2.5 times in a decade.<br> 2)The UNWTO predicts that India will account for 50 million outbound tourists by 2020",

            ],
            "bestPlace":["Every state has it's own importance. Based on our knowledge I'll recommend you the best place for travel."],
            "bestCountry":["I'll recommend you the best destination according to weather."],
            "message1": ["Our automated appointment reminders increase patient satisfaction and build loyalty by providing them with convenient, non invasive reminders that are tailored to their specific needs and schedule! Customized to your patients and your office."],
            "message2": ["Personalize your communication with the Zingit Solution birthday club. Each year your patients will receive a customized birthday card sent right to their phone."],
            "message3": ["Broadcast alerts allow you to reach out to a set group of patients with important information that needs to be shared immediately."],

             "message4": ["Ensuring your patients receive the continued care that they need can be challenging and time consuming. "],
         "message5": ["Zingit Solutions will help you create a premium mobile website for your practice"],
             "message6": ["By using the Zingit Solutions Review feature, get patient reviews posted to Google, Yelp, Yahoo!"],
             "message7": ["Your 1 priority is your patients health. That is why it is important to continue their treatment through education."],
             "message8": ["It is no secret that it is far less costly to keep existing patient than acquire a new one."],
            "message9": ["Increasing patient satisfaction is not only done by showing your patients you care for their health, but that you also value their opinion."],
             "message10": ["Our true two way text conversations allow your front desk staff to communicate back and forth with the patient from the front desk computer for important things such as reschedules."],

}
    }
}


